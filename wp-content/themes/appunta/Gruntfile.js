  'use strict';

  var destination = "_site";
  var cwd = "production";
  var assets = 'assets/';
  var production = false;
  var url = "";


  module.exports = function (grunt) {
      grunt.initConfig({
          pkg: grunt.file.readJSON('package.json'),
          site: grunt.file.readYAML('site.yaml'),
          clean: [destination],
          assemble: {
              options: {
                  prettify: {
                      indent: 2
                  },
                  marked: {
                      sanitize: false
                  },
                  production: true,
                  data: '_data/*.{json,yml}',
                  assets: '_site/assets/',
                  assets_trailing_slash: true,
                  layoutdir: '_layouts',
                  partials: ['_includes/*.html'],
                  flatten: true
              },
              views: {
                  files: [
                      {
                          expand: true,
                          cwd: cwd,
                          src: ['**/*.*'],
                          dest: destination
        }
    ]
              }

          }, //close assemble//

          sass: {
              dist: {
                  options: {
                      trace: true,
                      style: 'expanded', //nested, compressed, expanded,compact,
                      update: true,
                      sourcemap: 'none'
                  },
                  files: [
                      {
                          expand: true,
                          cwd: assets + '/sass',
                          src: ['**.scss'],
                          dest: assets + '/css',
                          //dest:"smb://172.16.90.52/brusselslife-v4/sites/all/themes/brusselslife_front_end/css",
                          ext: ".css"
        },
        ],
              }
          },

          autoprefixer: {
              options: {
                  browsers: ['> 1%', 'Firefox ESR', 'Opera 12.1']
              },
              prefix: {
                  expand: true,
                  cwd: assets + '/css',
                  src: ['**.css'],
                  dest: assets + '/css'
              }
          },
          //copy assets to _site folder//
          copy: {
              main: {
                  files: [
                      {
                          expand: true,
                          src: ['assets/**', '!assets/sass/**'],
                          dest: destination
       },
       ],
              }
          },
          //inlinecss for email projects//
          inlinecss: {
              main: {
                  options: {
                      applyLinkTags: true,
                      applyStyleTags: true,
                      removeStyleTags: false
                  },
                  files: {
                      'email-project/inline.html': 'email-project/email.html' //inputfile//
                  }
              }
          },

          watch: {
              options: {
                  livereload: true,
                  debounceDelay: 50
              },
              sass: {
                  files: ['assets/sass/*.scss'],
                  tasks: ['sass', 'autoprefixer']

              },
              assemble: {
                  files: ['**/**', '!node_modules/**', '!' + destination + '/**'],
                  tasks: ['newer:copy', 'assemble']
              },
              inlinecss: {
                  files: 'email-project/index.html',
                  tasks: ['inlinecss']
              },
              concat:{
                  files:"assets/js/*.js",
                  tasks:['concat','newer:copy']
              }
          },
          express: {
              all: {
                  options: {
                      port: 9000,
                      hostname: "localhost",
                      bases: "_site",
                      livereload: true
                          //serverreload: true
                  }
              }

          },
          'ftp-diff-deployer': {
              options: {
                  //retry: 3
              },

              www: {
                  options: {
                      host: 'http://www.dev.brusselslife.be',
                      port: 21,
                      auth: {
                          username: 'eurokom_development',
                          password: 'dev123$'
                      },
                      src: '_site',
                      dest: 'web/brusselslife-v4.0/shan',
                      retry: 6,
                      diff: 'simple',
                      memory: 'brusselslife-ftp.json'
                  }
              }

          },
          
          
          
          concat:{
              
              options:{
                  
                  seperator:';'
                  
              },
              
    dist: {
      src: ['assets/js/*.js'],
      dest: 'assets/js/app/app.js',
    },
              
            
              
          }

      });

      grunt.loadNpmTasks('assemble');
      grunt.loadNpmTasks('grunt-newer');
      grunt.loadNpmTasks('grunt-contrib-clean');
      grunt.loadNpmTasks('grunt-contrib-sass'); //.scss //.sass//.less
      grunt.loadNpmTasks('grunt-contrib-copy');
      grunt.loadNpmTasks('grunt-contrib-watch');
      grunt.loadNpmTasks('grunt-inline-css');
      grunt.loadNpmTasks('grunt-contrib-concat');
      grunt.loadNpmTasks('grunt-autoprefixer');
      grunt.loadNpmTasks('grunt-express');
      grunt.loadNpmTasks('grunt-ftp-diff-deployer');
      grunt.registerTask('default', ['newer:clean','assemble','concat', 'sass', 'autoprefixer','newer:copy']);
      grunt.registerTask('ftp', ['clean', 'sass','concat', 'autoprefixer', 'copy', 'assemble', 'ftp-diff-deployer']);
      grunt.registerTask('email', ['inlinecss', 'watch:inlinecss', 'newer:copy']);
      grunt.registerTask('server', ['express', 'watch']);
      grunt.registerTask('email' ['inlinecss']);

  };